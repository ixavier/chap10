// cart.js
//
// Return a singleton shopping cart for the application facets to share.
//

class Cart {

    constructor(src) {

        this.entries = []

        if (src instanceof Cart) {

            this.entries = [ ...src.entries ]
        }
    }

    add(entry) {

        this.entries.push(entry)
    }

   remove(entry) {

        let index = this.entries.indexOf(entry)

        if (index >= 0) {

            this.entries.splice(index, 1)
        }
    }

    clear() {

        this.entries = []
    }

    total() {

        let sum = 0

        for (let entry of this.entries) {

            sum += entry.price
        }

        return sum
    }
}

export default Cart