// CartEntry
//
// An entry in the shopping cart. The price could be different, or there could be special instructions.
//

export default class CartEntry {

    constructor(src) {

        this.id = 0
        this.name = null
        this.price = null
        this.instructions = null

        if (src) {

            Object.assign(this, src)
        }
    }
}