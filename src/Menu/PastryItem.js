// NS40801 PastryItem.js
//

import React, { Component } from 'react';

import '../Assets/styles/application.css'

export default class PastryItem extends Component {

    render() {

        return (
            <tr>
                <td className="list-name">{this.props.pastry.name}</td>
                <td className="list-price">${this.props.pastry.price}</td>
                <td className="list-add-button"><button onClick={ () => this.props.addToCart(this.props.pastry) }>Add to cart</button></td>
           </tr>
        )
    }
}
