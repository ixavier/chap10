// PastryList.js
//

import React, { Component } from 'react';

import AccordianList from '../Common/AccordianList'
import '../Assets/styles/application.css'
import PastryItem from './PastryItem';

export default class PastryList extends Component {

    constructor(props, ...args) {

        super(props, ...args)
    
        this.state = {

            pastries: [],
            error: null
        }
    }

    render() {

        var response = null

        if (this.state.errors) {

            response = <span className="error">Unable to retrieve the list of pastries.</span>

        } else {

            let pastryItems = this.props.pastries.map( (pastry) => <PastryItem key={ pastry.id } pastry={ pastry } addToCart={ this.props.addToCart } /> )

            return (
                <AccordianList title="Pastries" open={ false }>
                    <table className="list">
                        <tbody>
                            <tr>
                                <th className="list-name"></th>
                                <th className="list-price">price</th>
                                <th className="list-add-button"></th>
                            </tr>
                            { pastryItems }
                        </tbody>
                    </table>
                </AccordianList>
            )
        }

        return response
    }
}
