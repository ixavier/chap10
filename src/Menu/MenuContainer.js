// MenuContainer.js
//

import { connect } from "react-redux";

import Menu from './Menu'
import OrderActionCreator from "../Model/OrderActionCreator";
import ProductActionCreator from "../Model/ProductActionCreator";

class MenuContainer {

    mapStateToProps(state, ownProps) {

        return {

            beverages: state.product.beverages,
            pastries: state.product.pastries,
            order: state.order
        }
    }

    mapDispatchToProps(dispatch, ownProps) {

        return {

            orderActionCreator: new OrderActionCreator(dispatch),
            productActionCreator: new ProductActionCreator(dispatch)
        }
    }

}

let mc = new MenuContainer()

export default connect(mc.mapStateToProps, mc.mapDispatchToProps)(Menu)