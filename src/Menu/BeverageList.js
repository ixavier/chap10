// BeverageList.js
//

import React, { Component } from 'react';

import AccordianList from '../Common/AccordianList'
import '../Assets/styles/application.css'
import BeverageItem from './BeverageItem';

export default class BeverageList extends Component {

    constructor(props, ...args) {

        super(props, ...args)
    
        this.state = {

            beverages: [],
            error: null
        }
    }

    render() {

        var response = null

        if (this.state.error) {

            response = <span className="error">Unable to retrieve the list of beverages.</span>
        
        } else {

            let beverageItems = this.props.beverages.map( (beverage) => <BeverageItem key={ beverage.id } beverage={ beverage } addToCart={ this.props.addToCart } /> )

            response = (
                <AccordianList title="Beverages" open={ false }>
                    <table className="list">
                        <tbody>
                            <tr>
                                <th className="list-name"></th>
                                <th className="list-price">price</th>
                                <th className="list-add-button"></th>
                            </tr>
                            { beverageItems }
                        </tbody>
                    </table>
                </AccordianList>
            )
        }

        return response
    }
}
