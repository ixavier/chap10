// Menu.js
//
// Cafe menu view.
//

import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import '../Assets/styles/application.css'
import BeverageList from './BeverageList'
import CartEntry from '../Cart/CartEntry';
import CartList from '../Checkout/CartList'
import PastryList from './PastryList'

class Menu extends Component {

    constructor(props) {

        super(props)

        this.addToCart = this.addToCart.bind(this)
        this.cancelAddToCart = this.cancelAddToCart.bind(this)
        this.changeSpecialInstructions = this.changeSpecialInstructions.bind(this)
        this.commitAddToCart = this.commitAddToCart.bind(this)

        this.state = {

            showSpecialInstructions: false,
            specialInstructions: '',
            item: null
        }

        this.props.productActionCreator.getBeverages()
        this.props.productActionCreator.getPastries()
    }

    render() {

        return (
            <div className="app-content">
                <div className={ this.state.showSpecialInstructions ? 'special-instructions-visible' : 'special-instructions-hidden' }></div>
                <div className={ this.state.showSpecialInstructions ? 'special-instructions' : 'special-instructions-hidden' }>
                    { this.state.item ? this.state.item.name : null }<br/><br/>
                    <input className="special-instructions" type="text" placeholder="Special Instructions" value={ this.state.specialInstructions } onChange={ this.changeSpecialInstructions } /><br/>
                    <div className="special-instructions-buttons">
                        <button onClick={ this.cancelAddToCart }>Cancel</button>
                        <button onClick={ this.commitAddToCart }>Add to Cart</button>
                    </div>
                </div>
                <h1>Menu</h1>
                <BeverageList beverages={ this.props.beverages } addToCart={ this.addToCart } />
                <PastryList pastries={ this.props.pastries } addToCart={ this.addToCart } />
                <h2>Cart</h2>
                <CartList order={ this.props.order } orderActionCreator={ this.props.orderActionCreator }/>
                { this.props.order.entries.length ? <Link to="/checkout"><button>Checkout</button></Link> : null }
            </div>
        )
    }

    addToCart(item) {

        this.setState({ showSpecialInstructions: true, item: item })
    }

    cancelAddToCart() {

        this.setState({ showSpecialInstructions: false, specialInstructions: '' })
    }

    changeSpecialInstructions(event) {

        this.setState({ specialInstructions: event.target.value })
    }

    commitAddToCart() {

        this.props.orderActionCreator.addOrderEntry(new CartEntry({ name: this.state.item.name, price: this.state.item.price, instructions: this.state.specialInstructions }))
        this.setState({ showSpecialInstructions: false, specialInstructions: '' })
    }
}

export default Menu