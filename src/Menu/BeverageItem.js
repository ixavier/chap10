// BeverageItem.js
//

import React, { Component } from 'react';

import '../Assets/styles/application.css'

export default class BeverageItem extends Component {

    render() {

        return (
            <tr>
                <td className="list-name">{this.props.beverage.name}</td>
                <td className="list-price">${this.props.beverage.price}</td>
                <td className="list-add-button"><button onClick={ () => this.props.addToCart(this.props.beverage) }>Add to cart</button></td>
            </tr>
        )
    }
}
