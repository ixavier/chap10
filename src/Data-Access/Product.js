// Product.js
//

export default class Product {

    constructor(src) {

        this.id = 0
        this.name = ''
        this.price = 0

        Object.assign(this, src)
    }
}