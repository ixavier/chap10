// Checkout.js
// Copyright (c) 2017-2018 NextStep IT Training. All rights reserved.
//

import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import '../Assets/styles/application.css'
import CartList from './CartList'

class Checkout extends Component {

    constructor(...args) {

        super(...args)

        this.state = {

            name: '',
            cardNumber: ''
        }

        this.checkout = this.checkout.bind(this)
    }

    render() {

        let SubmitButton = this.props.order.entries.length && this.isValid() ? withRouter( (props) => <input type="button" value="Submit" onClick={ () => this.checkout(props) } /> ) : () => null

        return (
            <div>
                <h1>Checkout</h1>
                <CartList order={ this.props.order } orderActionCreator={ this.props.orderActionCreator } />
                <form>
                    { this.state.error ? <span className="cc-form-errors">Please provide the required values</span> : null }
                    <div className="cc-form">
                        <div className="cc-form-row">
                            <div className="cc-form-label"><label className="cc-form-label">Name:</label></div>
                            <div className="cc-form-field"><input type="text" className={ 'cc-form-field ' + ( this.state.name ? 'cc-form-field-requirement-met' : 'cc-form-field-required' ) } value={ this.state.name } onChange={ (event) => this.setState({ name: event.target.value })} /></div>
                            <div className="clear-all"></div>
                        </div>
                        <div className="cc-form-row">
                            <div className="cc-form-label"><label className="cc-form-label">Card Number:</label></div>
                            <div className="cc-form-field"><input type="text" className={ 'cc-form-field ' + ( this.state.cardNumber ? 'cc-form-field-requirement-met' : 'cc-form-field-required' ) } value={ this.state.cardNumber } onChange={ (event) => this.setState({ cardNumber: event.target.value })} /></div>
                            <div className="clear-all"></div>
                        </div>
                        <div className="cc-form-row">
                            <div className="cc-form-label"></div>
                            <div className="cc-form-field">
                                <Link to="/Menu"><button>Cancel</button></Link>&nbsp;
                                <SubmitButton />
                            </div>
                            <div className="clear-all"></div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

    checkout(props) {

        if (this.isValid()) {

            this.props.orderActionCreator.clearOrder()
            props.history.push("/Menu")
        }

        this.setState({ error: true })
    }

    isValid() {

        return this.state.name && this.state.cardNumber
    }
}

export default Checkout