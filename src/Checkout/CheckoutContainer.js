// CheckoutContainer.js
//

import { connect } from "react-redux";

import Checkout from './Checkout'
import OrderActionCreator from "../Model/OrderActionCreator";

class CheckoutContainer {

    mapStateToProps(state) {

        return {

            order: state.order
        }
    }

    mapDispatchToProps(dispatch) {

        return {

            orderActionCreator: new OrderActionCreator(dispatch)
        }
    }

}

let cc = new CheckoutContainer()

export default connect(cc.mapStateToProps, cc.mapDispatchToProps)(Checkout)