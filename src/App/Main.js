// Main.js
//

import React, { Component } from 'react' 
import { Route, Switch } from 'react-router'

import CheckoutContainer from '../Checkout/CheckoutContainer'
import Footer from '../Boilerplate/Footer'
import Header from '../Boilerplate/Header'
import Landing from '../Landing/Landing'
import MenuContainer from '../Menu/MenuContainer'
import Navigation from './Navigation'

class Main extends Component {

    render() {

        return (
            <div className="app">
                <Header />
                <Navigation />
                <div className="app-content">
                    <Switch>
                        <Route path="/" exact={ true } component={ Landing } />
                        <Route path="/menu" component={ MenuContainer } /> } />
                        <Route path="/checkout" component={ CheckoutContainer } /> } />
                    </Switch>
                </div>
                <Footer />
            </div>
        )
    }
X}

export default Main