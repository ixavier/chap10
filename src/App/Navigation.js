// Navigation.jsx
//

import React, { Component } from 'react'
import { withRouter } from 'react-router'

class Navigation extends Component {

    render() {

        let NavWithRouter = withRouter((props) => (

			<div className="navigation">
				<div id="home-button" className={ `${ props.location.pathname === '/' ? 'navbutton-selected' : 'navbutton' }` }
					onClick={ () => props.history.push('/') }>Home</div>
				<div id="menu-button" className={ `${ props.location.pathname === '/menu' ? 'navbutton-selected' : 'navbutton' }` }
					onClick={ () => props.history.push('/menu') }>Menu</div>
				<div id="checkout-button" className={ `${ props.location.pathname === '/checkout' ? 'navbutton-selected' : 'navbutton' }` }
					onClick={ () => props.history.push('/checkout') }>Checkout</div>
			</div>
		))

		return <NavWithRouter />
    }
}

export default Navigation