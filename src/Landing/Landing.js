// Landing.js
//
// Application landing page content.
//

import React, { Component } from 'react'

import '../Assets/styles/application.css'

class Landing extends Component {

    render() {

        let today = new Date()
        let message = null
    
        if (today.getHours() < 17) {
    
            message =   <span className="welcome-message">
                            { today.getHours() < 12 ? 'Good morning!' : 'Good afternoon!' }&nbsp;
                            Welcome to the Caribbean Coffee Company!
                        </span>
    
        } else {
    
            message =   <span className="welcome-message">
                            Welcome to the Caribbean Coffee Company! Sorry, we're closed.
                        </span>
        }

        return message
    }
}

export default Landing