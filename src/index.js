// index.js
// Copyright (c) 2017-2018 NextStep IT Training. All rights reserved.
//

import React from 'react'
import ReactDOM from 'react-dom'

import './Assets/styles/application.css'
import App from './App/App'

ReactDOM.render(<App />, document.getElementById('root'))