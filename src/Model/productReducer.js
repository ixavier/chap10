// productReducer.js
//

import { GET_BEVERAGES_ACTION, GET_PASTRIES_ACTION } from './modelActions'

class ProductReducer {

    constructor() {

        this.reduce = this.reduce.bind(this)
    }

    reduce(state, action) {

        let result = state ? state : null

        switch (action.type) {

            case GET_BEVERAGES_ACTION:
                result = this.reduceBeverages(result, action.data)
                break

            case GET_PASTRIES_ACTION:
                result = this.reducePastries(result, action.data)
                break

            default:
                break
        }

        return result
    }

    reduceBeverages(state, beverages) {

        let result = { ...state }

        result.beverages = beverages

        return result
    }

    reducePastries(state, pastries) {

        let result = { ...state }

        result.pastries = pastries

        return result
    }
}

export default (new ProductReducer()).reduce