// modelActions.js
//

export const GET_BEVERAGES_ACTION = 'get_beverages_action'
export const GET_PASTRIES_ACTION = 'get_pastries_action'

export const ADD_ORDER_ITEM_ACTION = 'add_order_item_action'
export const CLEAR_ORDER_ITEMS_ACTION = 'clear_order_items_action'
export const REMOVE_ORDER_ITEM_ACTION = 'remove_order_item_action'