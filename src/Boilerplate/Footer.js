// Footer.js
//
// Application footer content.
//

import React, { Component } from 'react'

import '../Assets/styles/application.css'

class Footer extends Component {

    render() {

        return (
            <footer className="app-footer">
                Copyright &copy; 2017-2018 NextStep IT Training. All rights reserved.
            </footer>
        );
    }
}

export default Footer